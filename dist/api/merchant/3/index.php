<?php

header('Access-Control-Allow-Origin: *');

// merchant
$data = array(
  'description' => "За определенные действия на сайте посетитель вашего сайта получает монетки соответствующего номинала. Примеры предложенных действий вы можете видеть ниже.",
  'how_to_win' => "Чтобы выиграть приз, клиент вводит специальный код. Попробуйте ввести следующий: m1c259b86170e3555, затем нажмите кнопку отправки формы.",
  'how_to_get_coupon' => "Код придет на электронную почту посетителя сайта после выполнения им определенных условий: регистрация, осуществление покупки, повторная покупка, пост в социальной сети и тому подобных.",
  'how_to_get_prize' => "Информация о том, как забрать подарок приходит на e-mail клиента!"
);

echo json_encode($data);
