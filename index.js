const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(express.static('dist'));
app.use(bodyParser.urlencoded({ extended: true }));

const data = {
  merchant: {
    description: "За определенные действия на сайте Вы будете получать монетки соответствующего номинала. В зависимости от номинала монеты Вы будете участвовать в розыгрыше более ценных товаров!",
    how_to_win: "Чтобы выиграть гарантированный приз, введите код полученный Вами в письме!",
    how_to_get_coupon: "В зависимости от номинала монеты Вы будете участвовать в розыгрыше более ценных товаров! За определенные действия на сайте Вы будете получать монетки соответствующего номинала.",
    how_to_get_prize: "Информация о том, как забрать подарок придет на Ваш email"
  },
  getWidget: {
    categories: [{
      name: "Одна звезда",
      description: "За предоставление личных данных (подписка на рассылку)"
    }, {
      name: "Две звезды",
      description: "За осуществление оплаты на сайте"
    }, {
      name: "Три звезды",
      description: "За выкупленный заказ"
    }, {
      name: "Четыре звезды",
      description: "За повторную покупку"
    }],
    products: [{
      "id": "68769876911",
      "name": "iWatch",
      "img": "https://itbukva.com/images/articles/1news/Next-Apple-Watch-to-be-called-iWatch.jpg"
    }, {
      "id": "68769833446",
      "name": "Powerbank",
      "img": "http://www.clasohlson.com/medias/sys_master/9347801612318.jpg"
    }, {
      "id": "687698769870",
      "name": "Футболка",
      "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Blue_Tshirt.jpg/220px-Blue_Tshirt.jpg"
    }, {
      "id": "687698729348",
      "name": "Чашка",
      "img": "https://target.scene7.com/is/image/Target/50524876?wid=520&hei=520&fmt=pjpeg"
    }, {
      "id": "687698769823",
      "name": "iPhone",
      "img": "http://hotline.ua/img/tx/107/1070040055.jpg"
    }, {
      "id": "687698712311",
      "name": "iWatch",
      "img": "https://itbukva.com/images/articles/1news/Next-Apple-Watch-to-be-called-iWatch.jpg"
    }, {
      "id": "68769815234875",
      "name": "Powerbank",
      "img": "http://www.clasohlson.com/medias/sys_master/9347801612318.jpg"
    }, {
      "id": "12763548999",
      "name": "Футболка",
      "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Blue_Tshirt.jpg/220px-Blue_Tshirt.jpg"
    }, {
      "id": "2934875623488",
      "name": "Чашка",
      "img": "https://target.scene7.com/is/image/Target/50524876?wid=520&hei=520&fmt=pjpeg"
    }, {
      "id": "17263541111",
      "name": "iPhone",
      "img": "http://hotline.ua/img/tx/107/1070040055.jpg"
    }, {
      "id": "309458677758",
      "name": "iWatch",
      "img": "https://itbukva.com/images/articles/1news/Next-Apple-Watch-to-be-called-iWatch.jpg"
    }, {
      "id": "123776766666",
      "name": "Powerbank",
      "img": "http://www.clasohlson.com/medias/sys_master/9347801612318.jpg"
    }, {
      "id": "098779999567",
      "name": "Футболка",
      "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Blue_Tshirt.jpg/220px-Blue_Tshirt.jpg"
    }]
  },
  products: {
    products: [{
      "id": "687698769876",
      "name": "Чашка",
      "img": "https://target.scene7.com/is/image/Target/50524876?wid=520&hei=520&fmt=pjpeg"
    }, {
      "id": "687698769872",
      "name": "iPhone",
      "img": "http://hotline.ua/img/tx/107/1070040055.jpg"
    }, {
      "id": "68769876911",
      "name": "iWatch",
      "img": "https://itbukva.com/images/articles/1news/Next-Apple-Watch-to-be-called-iWatch.jpg"
    }, {
      "id": "68769833446",
      "name": "Powerbank",
      "img": "http://www.clasohlson.com/medias/sys_master/9347801612318.jpg"
    }, {
      "id": "687698769870",
      "name": "Футболка",
      "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Blue_Tshirt.jpg/220px-Blue_Tshirt.jpg"
    }, {
      "id": "687698729348",
      "name": "Чашка",
      "img": "https://target.scene7.com/is/image/Target/50524876?wid=520&hei=520&fmt=pjpeg"
    }, {
      "id": "687698769823",
      "name": "iPhone",
      "img": "http://hotline.ua/img/tx/107/1070040055.jpg"
    }, {
      "id": "687698712311",
      "name": "iWatch",
      "img": "https://itbukva.com/images/articles/1news/Next-Apple-Watch-to-be-called-iWatch.jpg"
    }, {
      "id": "68769815234875",
      "name": "Powerbank",
      "img": "http://www.clasohlson.com/medias/sys_master/9347801612318.jpg"
    }, {
      "id": "12763548999",
      "name": "Футболка",
      "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Blue_Tshirt.jpg/220px-Blue_Tshirt.jpg"
    }, {
      "id": "2934875623488",
      "name": "Чашка",
      "img": "https://target.scene7.com/is/image/Target/50524876?wid=520&hei=520&fmt=pjpeg"
    }, {
      "id": "17263541111",
      "name": "iPhone",
      "img": "http://hotline.ua/img/tx/107/1070040055.jpg"
    }, {
      "id": "309458677758",
      "name": "iWatch",
      "img": "https://itbukva.com/images/articles/1news/Next-Apple-Watch-to-be-called-iWatch.jpg"
    }, {
      "id": "123776766666",
      "name": "Powerbank",
      "img": "http://www.clasohlson.com/medias/sys_master/9347801612318.jpg"
    }, {
      "id": "098779999567",
      "name": "Футболка",
      "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Blue_Tshirt.jpg/220px-Blue_Tshirt.jpg"
    }],
    prize: "123776766666"
  }
};

app.get('/api/merchants/:merchantId', (req, res) => {
  res.json(JSON.stringify(data.merchant));
}).get('/api/merchants/:merchantId/products', (req, res) => {
  res.json(JSON.stringify(data.getWidget));
}).post('/api/merchants/:merchantId/spin', (req, res) => {
  res.json(data.products);
}).post('/api/', (req, res) => {
  res.send('Спасибо! Ваше сообщение принято!');
});

app.listen(3000);
