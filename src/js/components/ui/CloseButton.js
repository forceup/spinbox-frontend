import React from 'react';
import $ from 'jquery';

export class CloseButton extends React.Component {
  constructor(props) {
    super(props);
    this.spinTriggerClose = this.spinTriggerClose.bind(this);
  }
  spinTriggerClose() {
    $('body').removeClass('spin-appear spin-details spin-rolling spin-roulette spin-win');
    $('.spin').find('[disabled]').removeAttr('disabled');
  }
  render() {
    return(
        <button onClick={this.spinTriggerClose} className="spin-close" type="button">
          <svg width="14" height="14">
            <use xlinkHref={"#spinbox-close"} />
          </svg>
        </button>
    );
  }
}
