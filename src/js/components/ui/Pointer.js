import React from 'react';

export class Pointer extends React.Component {
  render() {
    return(
        <div className="spin-pointer">
          <div className="spin-loader"><span> </span><span> </span><span> </span></div>
        </div>
    );
  }
}
