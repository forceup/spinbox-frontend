import React from 'react';

export class Logo extends React.Component {
  render() {
    return (
        <div className="spin-logo">
          <svg width="46" height="31">
            <use xlinkHref={"#spinbox-logo"} />
          </svg>
          <div className="spin-logo-name">Spinbox</div>
        </div>
    )
  }
}
