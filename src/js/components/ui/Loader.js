import React from 'react';

export class Loader extends React.Component {
  render() {
    return(
        <div className="spin-loader"><span> </span><span> </span><span> </span></div>
    );
  }
}
