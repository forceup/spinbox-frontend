import React from 'react';

export class Firework extends React.Component {
  render() {
    return (
        <div className="spin-firework">
          {[1,2,3,4,5,6].map(i =>
              <div className="spin-shine" key={i}>
                <svg width="25.88" height="32.11">
                  <use xlinkHref={"#spinbox-shine"} />
                </svg>
              </div>
          )}
        </div>
    )
  }
}
