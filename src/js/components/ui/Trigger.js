import React from 'react';
import $ from 'jquery';

export class Trigger extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return(
        <button onClick={this.props.getProducts} className="spin-button" type="button" />
    );
  }
}
