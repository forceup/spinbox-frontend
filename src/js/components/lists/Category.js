import React from 'react';

export class Category extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
        <div className="spin-coin">
          <div className="spin-rating">
            {Array.apply(null, new Array(this.props.index + 1)).map((el, i) =>
                <div className="spin-star" key={i}>
                  <svg width="24" height="22">
                    <use xlinkHref={"#spinbox-star"} />
                  </svg>
                </div>
            )}
          </div>
          <div className="spin-coin-desc">{this.props.description}</div>
          <div className="spin-coin-name">{this.props.name}</div>
        </div>
    )
  }
}
