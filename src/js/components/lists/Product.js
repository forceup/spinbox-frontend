import React from 'react';

export class Product extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
        <figure id={this.props.id} className="spin-prize">
          <div className="spin-prize-inner">
            <img src={this.props.img} />
            <figcaption className="spin-prize-name">{this.props.name}</figcaption>
          </div>
        </figure>
    )
  }
}
