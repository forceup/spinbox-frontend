import css from '../css/style.scss';

import $ from 'jquery';
import {TimelineMax, TweenMax} from 'gsap';

import React from 'react';
import ReactDOM from 'react-dom';
import { Category, Product, Firework, Vectors, Logo,
  Trigger, Loader, Pointer, CloseButton } from "./components";

// helper function
function getTanDeg(deg) { return Math.tan(deg * Math.PI/180) }

// spinbox host
let spinHost = 'https://spinbox.pro/api/merchants/';

class Spinbox extends React.Component {

  constructor(props) {
    super(props);

    // Setting up main state
    this.state = {
      products: [],
      categories: [],
      tooltip: false,
      prizeName: '',
      prizeId: '',
      error: ''
    };

    // Binding methods to component
    this.spin = this.spin.bind(this);
    this.submit = this.submit.bind(this);
    this.unError = this.unError.bind(this);
    this.tooltip = this.tooltip.bind(this);
    this.showDetails = this.showDetails.bind(this);
    this.getProducts = this.getProducts.bind(this);
    this.updatePrizesUI = this.updatePrizesUI.bind(this);

  }

  // Lifecycle methods

  // Setting up helper values
  componentWillMount() {
    this.setState({
      merchant: this.props.state
    });
  }

  // Refresh Spinbox
  componentDidMount() {
    this.updatePrizesUI();

    $('.spin-button').addClass('spin-button-appear');
    TweenMax.to('.spin-roller-rolla', 2, {
      rotation: 360,
      delay: .5
    });

  }

  // Refresh Spinbox too :)
  componentDidUpdate() {
    this.updatePrizesUI();
  }

  // Refresh Spinbox method
  updatePrizesUI() {
    const prizes = $('.spin-prizes'), prize = $('.spin-prize'),
        prizeAll = prize.length, prizeAngle = 360 / prizeAll;
    const prizeRadius = prize.outerWidth() / 2 / getTanDeg(prizeAngle / 2);
    prize.each(function(index) {
      $(this)
          .attr('style', `transform: rotateY(${prizeAngle * index}deg) translateZ(${prizeRadius}px)`)
          .attr('data-angle', prizeAngle * index);
    });
    TweenMax.to('.spin-prizes', 1, {
      ease: Circ.easeInOut,
      z: -prizeRadius,
      rotationY: 0
    });
  }

  // Toggle tooltip
  tooltip() {
    this.setState({
      tooltip: !this.state.tooltip
    });
  }

  // Render prizes to spin
  submit(event) {
    event.preventDefault();
    const component = this, form = component.form;
    component.unError();
    $(form).addClass('spin-loading');

    $.ajax({
      type: "POST",
      dataType: 'json',
      url: spinHost + '/spin',
      data: {
        code: component.coupon.value,
        email: component.email.value
      },
      success(data) {
        setTimeout(e => {
          if (data.error) {
            $(form).removeClass('spin-loading');
            component.setState({ error: data.error });
            return;
          }
          component.setState({
            products: data.products,
            prizeId: data.prize,
            prizeName: $(`[id="${data.prize}"]`).find('.spin-prize-name').text()
          }, then => component.updatePrizesUI());
          $(form).removeClass('spin-loading').closest('body')
              .addClass('spin-roulette').removeClass('spin-details');
          form.reset();
        }, 500);
      },
      error(err) {
        $(form).removeClass('spin-loading');
        component.setState({
          error: err.responseJSON ? err.responseJSON.error : "Неизвестная ошибка"
        });
      }
    });
  }

  unError() { this.setState({ error: '' }) }

  // Goto second step by scrolling carousel
  showDetails(e) {

    e.target.setAttribute('disabled', 'disabled');

    // const timeline = new TimelineMax({
    //   onComplete() {
    //     $('body').addClass('spin-details');
    //   }
    // });
    //
    // timeline.to('.spin-prizes', 2, {
    //   ease: Power4.easeIn,
    //   rotationY: "-=360"
    // }).to('.spin-prizes', 2, {
    //   ease: Power4.easeOut,
    //   rotationY: "-=360"
    // });

    $('body').addClass('spin-details');

  }

  // Main Spinner function
  spin() {
    $('body').addClass('spin-rolling');

    const timeline = new TimelineMax(),
        prizeAngle = $(`[id="${this.state.prizeId}"]`).data('angle');

    timeline.to('.spin-prizes', 2, {
      ease: Expo.easeIn,
      rotationY: "-=360"
    }).to('.spin-prizes', 1, {
      ease: Power0.easeNone,
      rotationY: "-=360",
      repeat: 2
    }).to('.spin-prizes', 4, {
      rotationY: "-=" + prizeAngle,
      ease: Power4.easeOut,
      onComplete() { $('body').removeClass('spin-roulette spin-rolling').addClass('spin-win') }
    });
  }

  // get products
  getProducts() {
    $('.spin').addClass('spin-loading');
    $.get(spinHost + '/products').done(data => {
      this.setState({ products: data.products, categories: data.categories });
      $('.spin').removeClass('spin-loading');
      $('body').addClass('spin-appear');
    });
  }

  // Render Spinbox Component
  render() {
    return(
        <div className="spin">
          <Trigger getProducts={this.getProducts} />
          <div className="spin-overlay">
            <div className="spin-steps">
              <Pointer />
              <div className="spin-step spin-step-main">
                <div className="spin-lottery">
                  <div className="spin-box">
                    <div className="spin-prizes">
                      { this.state.products.map((pr) =>
                          <Product name={pr.name} key={pr.id} img={pr.img} id={pr.id} />)
                      }
                    </div>
                  </div>
                  <div className="spin-buttons">
                    <button onClick={this.showDetails} className="spin-btn" type="button">Мне повезет!</button>
                    <button onClick={this.spin} className="spin-btn spin-btn-filled spin-btn-lucky" type="button">Играть</button>
                  </div>
                  <div className="spin-success">
                    <Firework />
                    <div className="spin-congrats">Вы выиграли приз: {this.state.prizeName}!</div>
                    <div className="spin-howto">{this.state.merchant.how_to_get_prize}</div>
                  </div>
                </div>
              </div>
              <div className="spin-step spin-step-details">
                <div className="spin-box">
                  <div className="spin-text">{this.state.merchant.description}</div>
                  <div className={"spin-coupon " + (this.state.tooltip ? "spin-tooltip-opened" : "")}>
                    <form ref={el => this.form = el} onSubmit={this.submit} className="spin-form" action="http://localhost/spinbox/src/api.php">
                      <div className="row">
                        <div className="spin-fields">
                          <p>{this.state.merchant.how_to_win}</p>
                        </div>
                      </div>
                      <div className="row">
                        <div className="spin-fields">
                          <input type={'email'} onFocus={this.unError} autoComplete={'off'} spellCheck={false} ref={el => this.email = el} required className="input" placeholder="Ваш e-mail" name="email" />
                        </div>
                      </div>
                      <div className="row">
                        <div className={"spin-error " + (this.state.error.length ? "spin-error-appear" : "")}>{this.state.error}</div>
                        <fieldset className="spin-fields">
                          <input onFocus={this.unError} minLength={10} autoComplete={'off'} spellCheck={false} ref={el => this.coupon = el} required className="input" placeholder="Введите полученный код" name="coupon" />
                          <button className="submit">
                            <svg width="29" height="20">
                              <use xlinkHref={"#spinbox-submit"} />
                            </svg>
                          </button>
                        </fieldset>
                      </div>
                      <div className="row">
                        <div className="spin-help">
                          <button onClick={this.tooltip} className="spin-tooltip-open" type="button">Где получить код?</button>
                          <div className="spin-tooltip">
                            <p>{this.state.merchant.how_to_get_coupon}</p>
                            <button onClick={this.tooltip} className="spin-close" type="button">
                              <svg width="14" height="14">
                                <use xlinkHref={"#spinbox-close"} />
                              </svg>
                            </button>
                          </div>
                        </div>
                      </div>
                      <div className="spin-loader"><span> </span><span> </span><span> </span></div>
                    </form>
                  </div>
                </div>
              </div>
              <CloseButton />
            </div>
          </div>
          <Loader />
          <Vectors />
        </div>
    );
  }
}

$(function() {
  "use strict";
  const merchant = $('script[data-merchant]');
  if (merchant.length) {
    const merchantID = merchant.data('merchant');
    $.get(spinHost += merchantID)
        .done(data => {
          $('body').append('<div id="spin-widget"></div>');
          ReactDOM.render(<Spinbox merchant={merchantID} state={data} />,
              document.getElementById('spin-widget'));
        });
  }
});
